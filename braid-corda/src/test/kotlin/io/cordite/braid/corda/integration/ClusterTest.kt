/**
 * Copyright 2018 Royal Bank of Scotland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.integration

import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.getOrThrow
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.PortAllocation
import net.corda.testing.driver.driver
import org.junit.Test

class ClusterTest {
  companion object {
    private val log = contextLogger()
  }

  @Test
  fun test() {
    val portAllocation = PortAllocation.defaultAllocator

    driver(
      DriverParameters(
        isDebug = false,
        startNodesInProcess = true,
        portAllocation = portAllocation
      )
    ) {
      // start up the controller and all the parties
      val nodeHandle = startNode(providedName = CordaX500Name("party", "London", "GB")).getOrThrow()
      println("hello node info: ${nodeHandle.nodeInfo}")
      nodeHandle.stop()
    }
  }
}
<!--
  ~ Copyright 2018 Royal Bank of Scotland
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <artifactId>braid</artifactId>
    <groupId>io.cordite.braid</groupId>
    <version>4.4.0-SNAPSHOT</version>
    <relativePath>../pom.xml</relativePath>
  </parent>
  <artifactId>braid-quickstart</artifactId>
  <name>braid-quickstart</name>
  <packaging>jar</packaging>

  <licenses>
    <license>
      <name>The Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:git://gitlab.com/bluebank/braid.git</connection>
    <developerConnection>scm:git:ssh://gitlab.com:bluebank/braid.git</developerConnection>
    <url>http://gitlab.com/bluebank/braid/tree/master</url>
  </scm>

  <developers>
    <developer>
      <name>Farzad Pezeshkpour</name>
      <email>fuzz@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Chris Jones</name>
      <email>chris.jones@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Ben Wyeth</name>
      <email>ben.wyeth@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Mark Simpson</name>
      <email>mark.simpson@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
    <developer>
      <name>Ramiz Amad</name>
      <email>ramiz.amad@bluebank.io</email>
      <organization>Royal Bank of Scotland</organization>
      <organizationUrl>https://www.rbs.com</organizationUrl>
    </developer>
  </developers>

  <dependencies>
    <dependency>
      <groupId>io.cordite.braid</groupId>
      <artifactId>braid-standalone-server</artifactId>
      <version>${project.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
        <includes>
          <include>META-INF/maven/archetype-metadata.xml</include>
        </includes>
      </resource>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>false</filtering>
        <excludes>
          <exclude>META-INF/maven/archetype-metadata.xml</exclude>
        </excludes>
      </resource>
    </resources>
    <testResources>
      <testResource>
        <directory>src/test/resources</directory>
        <filtering>true</filtering>
        <includes>
          <include>projects/it1/archetype.properties</include>
        </includes>
      </testResource>
      <testResource>
        <directory>src/test/resources</directory>
        <filtering>false</filtering>
        <excludes>
          <exclude>projects/it1/archetype.properties</exclude>
        </excludes>
      </testResource>
    </testResources>
    <plugins>
      <plugin>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-maven-plugin</artifactId>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-archetype-plugin</artifactId>
        <version>3.0.1</version>
        <executions>
          <execution>
            <id>integration-test</id>
            <phase>verify</phase>
            <goals>
              <goal>integration-test</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-resources-plugin</artifactId>
        <version>3.0.2</version>
      </plugin>
    </plugins>
  </build>
  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <reportSets>
          <reportSet>
            <inherited>false</inherited>
            <reports/>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>kr.motd.maven</groupId>
        <artifactId>sphinx-maven-plugin</artifactId>
        <version>2.2.2</version>
        <reportSets>
          <reportSet>
            <inherited>false</inherited>
          </reportSet>
        </reportSets>
      </plugin>
    </plugins>
  </reporting>
</project>